# !/bin/bash -i
##################################################################################
# install.sh   bash script to automate installation of Haskell katas.            #
# Tested on a 20.04 LTS (Focal Fossa) and 22.04 (Jammy Jellyfish)                #
#                                                                                #
#   asciinema rec log.out -c "bash -i -c ./install.sh" || ./install.sh           #
#                                                                                #
# CC-BY-SA HaskellKatas. Mercedes Cordero, Reynaldo Cordero (06-06-2022)         #
##################################################################################

INSTALL_STEP_BY_STEP=""
#INSTALL_STEP_BY_STEP="Yes"    # uncomment if you want a install "step by step"

U="\033[4m"  # underline ON
B="\033[1m"  # bold ON
b="\033[0m"  # bold OFF  (every other mark is OFF too)

VERSION='v0.30'

LOG="log.out"
ASCIINEMA_COMMAND="${B}asciinema rec --overwrite ${LOG} -c \"bash -i -c ${0}\"${b}"

NAME=""
EMAIL=""
COPYRIGHT='Copyright (c) YOURNAME'
GITHUB_USERNAME='YOURUSERNAME'

RESOLVER=lts-19.10  # https://www.stackage.org/lts-19.10

function yesOption()
{
  if [[ -z "${INSTALL_STEP_BY_STEP}" ]] ; then
    echo -n " -y"
  fi
}


function processYNAbort_es()
{
  echo -e "[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' o Ctrl-C para abortar]"
  trap "exit 1" INT
  if [ "$*" == "" ]; then
    echo -n "[S/n/(a)bortar] "
    if [[ ! -z "${INSTALL_STEP_BY_STEP}" ]] ; then
      read answ
    else
      echo
      echo "==> S"
      answ="S"
    fi
    echo "################################################################################"
    if [ "${answ}" == "" ] || [ "${answ}" == "s" ] || [ "${answ}" == "S" ]; then
      :
    elif [ "${answ}" == "a" ] || [ "${answ}" == "A" ] || [ "${answ}" == "abort" ] || [ "${answ}" == "n" ] || [ "${answ}" == "N" ] ; then
      exit 0
    else
      processYNAbort_es "$*"
    fi
  else
    echo "*** Ejecutamos: ($*)?"
    echo -n "[S/n/(a)bortar] "
    if [[ ! -z "${INSTALL_STEP_BY_STEP}" ]] ; then
      read answ
    else
      echo
      echo "=> S"
      answ="S"
    fi
    echo "################################################################################"
    if [ "${answ}" == "" ] || [ "${answ}" == "s" ] || [ "${answ}" == "S" ]; then
      echo "$@"
      "$@"
    elif [ "${answ}" == "n" ] || [ "${answ}" == "N" ] ; then
      echo "Se salta este paso."
    elif [ "${answ}" == "a" ] || [ "${answ}" == "A" || [ "${answ}" == "abortar" ] ; then
      exit 1
    else
      processYNAbort_es "$*"
    fi
  fi
}


## Mensajes de inicio
##
echo -e -n "

${B}Script de instalaci\u00f3n de entorno de programaci\u00f3n ${U}'newk'${b}${B}
  para katas con Haskell.${b}  ${U}${VERSION}${b}.

https://gitlab.com/HaskellKatas/katas--proof-of-concept

Funciona de forma nativa, o en una m\u00e1quina virtual, en un
  ${B}Ubuntu 20.04 LTS${b} (Focal Fossa) o posterior.

Consta de 21 pasos, la mayor\u00eda r\u00e1pidos, excepto el \u00faltimo.

[pulsa 'Retorno' para comenzar, o bien Ctrl-C para salir] "

read
echo "################################################################################"

which asciinema >/dev/null 2>&1
if [ "$?" -ne 0 ] ; then
  echo -e "
${B}'asciinema'${b} NO est\u00e1 instalado.
Se utiliza para guardar un log de la instalaci\u00f3n, lo que es muy recomendable
  como documentaci\u00f3n y para el diagn\u00f3stico de problemas de instalaci\u00f3n."

## Instalar asciinema
## https://command-not-found.com/asciinema

  echo -e "
${B}0) Instalar asciinema${b}  (Grabar la sesi\u00f3n de instalaci\u00f3n, a modo de archivo de log)"

  if [[ -z "${INSTALL_STEP_BY_STEP}" ]] ; then
    INSTALL_STEP_BY_STEP="Yes"
    processYNAbort_es "sudo" "apt" "install" "asciinema"
    INSTALL_STEP_BY_STEP=""
  else
    processYNAbort_es "sudo" "apt" "install" "asciinema"
  fi
  echo
  which asciinema >/dev/null 2>&1
  if [ "$?" -eq 0 ] ; then
    echo
    echo -e "asciinema reci\u00e9n instalado"
    echo -e "${B}Si quiere usarlo (recomendado)${b} debe ejecutar lo siguiente: "
    echo
    echo -e "${ASCIINEMA_COMMAND}"
    echo
    exit 1
  fi
fi

pgrep -x "asciinema" > /dev/null 2>&1
if [ "$?" -eq 0 ] ; then
  echo
  echo -e "Se est\u00e1 registrando un archivo de log de la instalaci\u00f3n:

       ${B}${LOG}${b}"
else
  which asciinema >/dev/null 2>&1
  if [ "$?" -eq 0 ] ; then
    echo -e "
${B}'asciinema'${b} est\u00e1 instalado."
  else
    echo -e "
${B}'asciinema'${b} NO est\u00e1 instalado."
  fi
  echo
  echo -ne "Puede ${B}continuar, sin guardar el log${b}, pulsando la tecla '${B}Retorno${b}',
  o bien puede ${B}salir${b} pulsando '${B}Ctrl-C${b}' y ejecutar.

${ASCIINEMA_COMMAND}

[pulsa 'Retorno' para seguir, o Ctrl-C para salir, e intentarlo de nuevo] "
  read ; echo "##################################################"
fi


## Actualizar antes de instalar
##
echo -e "

${B}1) Actualizar el sistema${b} antes de instalar sus dependencias
[pulsa 'S', 's' o 'Retorno' para actualizar,
       'n' para saltar la actualizaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "update" $(yesOption)
processYNAbort_es "sudo" "apt" "upgrade" $(yesOption)


function getNameAndEmail()
{
  trap "exit 1" INT
  if [[ ! -z "${NAME}" ]] && [[ ! -z "${EMAIL}" ]] ; then
    echo
    echo "Se utilizan los ya aportados:"
    echo -e "Nombre y apellidos: ${B}${NAME}${b}"
    echo -e "Direcci\u00f3n de correo electr\u00f3nico: ${B}${EMAIL}${b}"
    echo
  else
    echo
    echo -n "Nombre y apellidos: "
    read NAME
    echo
    echo -n "Direcci\u00f3n de correo electr\u00f3nico: "
    read EMAIL
    if [[ ! -z "${NAME}" ]] && [[ ! -z "${EMAIL}" ]] ; then
      echo
      echo -e "Nombre y apellidos: ${B}${NAME}${b}"
      echo -e "Direcci\u00f3n de correo electr\u00f3nico: ${B}${EMAIL}${b}"
      echo "OK? [S/n]"
      echo -n "[S/n/(a)bortar] " ; read answ
      echo "################################################################################"
      if [ "${answ}" == "" ] || [ "${answ}" == "s" ] || [ "${answ}" == "S" ]; then
        :
      elif [ "${answ}" == "a" ] || [ "${answ}" == "A" ] || [ "${answ}" == "abort" ]; then
        exit 1
      else
        echo "Vuelva a intentarlo, por favor."
        getNameAndEmail
      fi
    else
      echo "Vuelva a intentarlo, por favor."
      getNameAndEmail
    fi
  fi
}

function gitConfig()
{
  git config user.name >/dev/null 2>&1 && git config user.email >/dev/null 2>&1
  if [ "$?" -ne 0 ] ; then
    echo
    echo "'git' necesita su nombre y apellidos y su direcci\u00f3n de correo electr\u00f3nico."
    getNameAndEmail

    git config --global user.name "${NAME}"
    git config --global user.email "${EMAIL}"
    git config user.name >/dev/null 2>&1
    if [ "$?" -ne 0 ] ; then
      echo
      echo "'git' no ha podido configurar su nombre."
      echo
      gitConfig
    fi
    git config user.email >/dev/null 2>&1
    if [ "$?" -ne 0 ] ; then
      echo
      echo "'git' no ha podido configurar su direcci\u00f3n de correo."
      echo
      gitConfig
    fi
  fi
}


## Instalar git
## https://command-not-found.com/git
## https://linuxize.com/post/how-to-install-git-on-debian-9/
echo -e "

${B}2) Instalar git${b}  (Sistema de control de versiones de archivos)
   y hacer una configuraci\u00f3n m\u00ednima
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "git" $(yesOption)

gitConfig


function cloneRepo()
{
  TMP=$(mktemp -d "${TMPDIR:-/tmp/}$(basename $0).XXXXXXXXXXXX")
  REPO=${TMP}/katas--proof-of-concept
  cd ${TMP}
  git clone https://gitlab.com/HaskellKatas/katas--proof-of-concept
  cd "katas--proof-of-concept/config/editor/emacs/"
  if [ "$?" -ne 0 ] ; then
    echo
    echo "Problema al clonar temporalmente el repositorio
  'https://gitlab.com/HaskellKatas/katas--proof-of-concept'"
    echo "No se puede recuperar el programa newk ni los archivos
  de configuraci\u00f3n propuestos."
    exit 1
  fi
}

cloneRepo


function updateProfile()
{
  grep '${HOME}/bin'  ~/.profile >/dev/null 2>&1
  if [ "$?" -ne 0 ] ; then
    if [[ ! ${PATH} =~ "${HOME}/bin" ]] ; then
      NEWPATH='${HOME}/bin:${PATH}'
      echo "" >> ${HOME}/.profile
      echo $NEWPATH >> ${HOME}/.profile
      echo
      echo "PATH modificado en el archivo '.profile':"
      echo ${NEWPATH}
      echo
      echo "Cuando termine la instalaci\u00f3n del todo, salga de la sesi\u00f3n
para que el nuevo PATH sea efectivo."
    # echo "Si lo desea, puede ejecutar al final de la instalaci\u00f3n:    source ~/.profile"
    fi
  fi
  grep 'export PATH'  ${HOME}/.profile >/dev/null 2>&1
  if [ "$?" -ne 0 ] ; then
    echo "" >> ${HOME}/.profile
    echo "export PATH" >> ${HOME}/.profile
    echo
    echo "'export PATH' a\u00f1adido al archivo '.profile':"
    echo
    echo "Cuando termine la instalaci\u00f3n del todo, salga de la sesi\u00f3n
  para que el nuevo PATH sea efectivo."
  fi
}

function _installscripts()
{
  BIN=~/bin
  NEWK=newk
  NEWK2=${NEWK}-$(date +%Y-%m-%d-%H-%M-%S)
  cd ${REPO}
  chmod +x ${NEWK}
  if [ -e ${BIN} ]; then
    if [ ! -d ${BIN} ]; then
      echo
      echo "El programa newk no puede guardarse en el directorio ${BIN}."
      echo
      echo "Actualmente ${BIN} es:"
      echo
      ls -l ${BIN}
      echo
      exit 1
    fi
  else
    mkdir ${BIN}
  fi
  if [ -e ${BIN}/${NEWK} ]; then
    if [ ! -f ${BIN}/${NEWK} ]; then
      echo
      echo "El programa ${NEWK} no puede guardarse en el directorio ${BIN}."
      echo
      echo "Actualmente ${NEWK} es:"
      echo
      ls -l ${BIN}/${NEWK}
      echo
      exit 1
    else
      echo
      echo "Se renombrar\u00e1 el actual ${BIN}/${NEWK} como ${BIN}/${NEWK2}"
      echo
      mv ${BIN}/${NEWK} ${BIN}/${NEWK2}
    fi
  fi
  cp -i ${NEWK} ${BIN}
  echo "PATH actual: "
  echo ${PATH}
  echo $(echo ${PATH}) | grep "${BIN}" -- 2>&1
  if [ "$?" -ne 0 ] ; then
    updateProfile
  fi
}


## Instalar bash scripts de la aplicaci\u00f3n newk
## https://gitlab.com/HaskellKatas/katas--proof-of-concept
echo -e "

${B}3) Instalar la aplicaci\u00f3n 'newk'${b} (bash script)
   en directorio '~/bin' y comprobar el PATH
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "_installscripts"


function _installEmacsConfig()
{
  DIR=~/.emacs.d/
  DIR2=~/.emacs.d-$(date +%Y-%m-%d-%H-%M-%S)/
  DIR3=${REPO}/config/editor/emacs/.emacs.d/
  if [ -d $DIR ]; then
    echo
    echo "\u00bfQuiere cambiar la configuraci\u00f3n de emacs actual por una con soporte para Haskell (Dante)?"
    echo
    echo "Se renombrar\u00e1 ${DIR} como ${DIR2}"
    echo
    processYNAbort_es

    mv ${DIR} ${DIR2}
    if [ "$?" -ne 0 ] ; then
      echo
      echo "Error al renombrar. Revise el archivo (\u00bfProblema con los permisos?)"
      echo
      ls -la ${DIR}
      echo
      exit 1
    fi
  fi
  echo cp -r ${DIR3} ${DIR}
  cp -r ${DIR3} ${DIR}
  if [ "$?" -ne 0 ] ; then
    echo
    echo "Error al actualizar la configuraci\u00f3n de emacs (~/.emacs.d)."
    echo
  else
    echo
    echo "Configuraci\u00f3n de emacs actualizada (~/.emacs.d)."
  fi
}


## Instalar emacs
## https://command-not-found.com/emacs
## https://www.howtoinstall.co/en/debian/stretch/emacs
echo -e "

${B}4) Instalar emacs${b}  (Editor de texto)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "emacs" $(yesOption)


echo -e "

${B}5) Instalar una configuraci\u00f3n de emacs${b} adaptada a las katas con Haskell"

processYNAbort_es "_installEmacsConfig"


## Instalar xdotool
## https://command-not-found.com/xdotool
## https://www.howtoinstall.co/en/debian/stretch/xdotool
echo -e "

${B}6) Instalar xdotool${b}  (Introduce pulsaciones de teclado autom\u00e1ticamente)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "xdotool" $(yesOption)


function _installXresources()
{
  FILE=~/.Xresources
  FILE2=~/.Xresources-$(date +%Y-%m-%d-%H-%M-%S)
  FILE3=${REPO}/config/terminal/xterm/.Xresources
  if [ -f $FILE ]; then
    echo
    echo "\u00bfQuiere cambiar la configuraci\u00f3n de xterm actual por una nueva?"
    echo
    echo "(Se renombrar\u00e1 ${FILE} como ${FILE2} y se copiar\u00e1 la nueva versi\u00f3n.)"
    echo
    processYNAbort_es

    mv ${FILE} ${FILE2}
    if [ "$?" -ne 0 ] ; then
      echo
      echo "Error al renombrar. Revise el archivo (\u00bfProblema con los permisos?)"
      echo
      ls -la ${FILE}
      echo
      exit 1
    fi
  fi
  cp ${FILE3} ${FILE}
  if [ "$?" -ne 0 ] ; then
    echo
    echo "Error al actualizar la configuraci\u00f3n de xterm (~/.Xresources)."
    echo
  else
    echo
    echo "Configuraci\u00f3n de xterm actualizada (~/.Xresources)."
  fi
  xrdb -merge ~/.Xresources
}


## Instalar xterm
## https://command-not-found.com/xterm
echo -e "

${B}7) Instalar xterm${b}  (Terminal de int\u00e9rprete de comandos)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "xterm" $(yesOption)

## Crear fichero de configuraci\u00f3n para xterm y cargarlo ~/.Xresources
##
echo -e "

${B}8) Instalar una configuraci\u00f3n de xterm (Xresources)${b} adaptada a las katas con Haskell
(tipo y tama\u00f1o de la fuente tipogr\u00e1fica)"

processYNAbort_es "_installXresources"


## Instalar fuentes DejaVu
## https://linux-packages.com/search-page?p=dejavu&st=contain
echo -e "

${B}9) Instalar fuentes DejaVu${b}  (para usar en el terminal de int\u00e9rprete de comandos)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "fonts-dejavu-core" $(yesOption)


## Instalar inotify-tools
## https://command-not-found.com/inotifywait
## https://www.howtoinstall.co/en/debian/stretch/inotify-tools
echo -e "

${B}10) Instalar inotify-tools${b}  (Monitoriza archivos y responde a cambios)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "inotify-tools" $(yesOption)

## Instalar wmctrl
## https://command-not-found.com/wmctrl
## https://www.howtoinstall.co/en/debian/stretch/inotify-tools
echo -e "

${B}11) Instalar wmctrl${b}  (Manejador de ventanas en el escritorio)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "wmctrl" $(yesOption)

## Instalar xclip
## https://command-not-found.com/xclip
echo -e "

${B}12) Instalar xclip${b}  (copiar-pegar desde la l\u00ednea de comandos)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "xclip" $(yesOption)

## Instalar meld
## https://command-not-found.com/meld
## https://www.howtoinstall.co/en/debian/stretch/meld
echo -e "

${B}13) Instalar meld${b}  (Compara archivos y permite editarlos)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "meld" $(yesOption)

function _updateBashrc()
{
  grep 'cdargs-bash.sh'  ~/.bashrc >/dev/null 2>&1
  if [ "$?" -ne 0 ] ; then
    echo "source /usr/share/doc/cdargs/examples/cdargs-bash.sh" >> ${HOME}/.bashrc
    echo
    echo "Se ha a\u00f1adido al archivo '.bashrc' la l\u00ednea: source /usr/share/doc/cdargs/examples/cdargs-bash.sh '"
    echo "Cuando termine la instalaci\u00f3n del todo, cierre la sesi\u00f3n para que el nuevo PATH sea efectivo."
    echo
  fi
}

## Instalar cdargs
## https://command-not-found.com/cdargs
## https://www.howtoinstall.co/en/debian/stretch/cdargs
echo -e "

${B}14) Instalar cdargs${b}  (Marcadores de directorios para navegaci\u00f3n r\u00e1pida)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "cdargs" $(yesOption)

echo -e "

${B}15) A\u00f1adir comandos de 'cdargs'${b} para la gesti\u00f3n de los marcadores en el archivo .bashrc
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "_updateBashrc"


function _installXbindkeysConfig()
{
  FILE=~/.xbindkeysrc
  FILE2=~/.xbindkeysrc-$(date +%Y-%m-%d-%H-%M-%S)
  FILE3=${REPO}/config/shortkeys/xbindkeys/.xbindkeysrc
  if [ -f $FILE ]; then
    echo
    echo "\u00bfQuiere cambiar la configuraci\u00f3n de xbindkeys actual por una nueva?"
    echo
    echo "Se renombrar\u00e1 ${FILE} como ${FILE2} antes del cambio."
    echo
    processYNAbort_es

    mv ${FILE} ${FILE2}
    if [ "$?" -ne 0 ] ; then
      echo
      echo "Error al renombrar. Revise el archivo (\u00bfProblema con los permisos?)"
      echo
      ls -la ${FILE}
      echo
      exit 1
    fi
  fi
  cp ${FILE3} ${FILE}
  if [ "$?" -ne 0 ] ; then
    echo
    echo "Error al actualizar la configuraci\u00f3n de xbindkeys (~/.xbindkeysrc)."
    echo
  else
    echo
    echo "Configuraci\u00f3n de xbindkeys actualizada (~/.xbindkeysrc)."
  fi
  killall -s1 xbindkeys 2> /dev/null ; xbindkeys -f ~/.xbindkeysrc
}

## Instalar xbindkeys
## https://command-not-found.com/xbindkeys
## https://www.linux.com/news/start-programs-pro-xbindkeys
echo -e "

${B}16) Instalar xbindkeys${b}  (Atajos de teclado para dar foco a ventanas, etc)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "xbindkeys" $(yesOption)

## Crear fichero de configuraci\u00f3n para xbindkeys y cargarlo ~/.xbindkeysrc
##
echo -e "

${B}17) Instalar una configuraci\u00f3n de xbindkeys${b} adaptada a las katas con Haskell"

processYNAbort_es "_installXbindkeysConfig"


## Instalar pygmentize
## https://command-not-found.com/pygmentize
echo -e "

${B}18) Instalar pygmentize${b}  (Colorear c\u00f3digo seg\u00fan sint\u00e1xis)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "python3-pygments" $(yesOption)


## Instalar wget
## https://command-not-found.com/wget
## https://www.howtoinstall.co/en/debian/stretch/wget
echo -e "

${B}19) Instalar wget${b} (Para descargar archivos.
   Se usa para instalar Haskell mediante 'stack')
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"

processYNAbort_es "sudo" "apt" "install" "wget" $(yesOption)

## instalar PCRE, ya que lo necesita 'stack' para cargar librer\u00edas
## https://pkgs.org/download/libpcre3
## apt-cache search pcre | grep -- -dev

echo -e "

${B}20) Instalar PCRE${b}  (Perl Compatible Regular Expressions.
   Esta librer\u00eda la necesita stack para preparar el entorno de Haskell)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]"
processYNAbort_es "sudo" "apt" "install" "libpcre3" "libpcre3-dev" "pkg-config" $(yesOption)

## Instalar stack
## https://docs.haskellstack.org/en/stable/install_and_upgrade/

# cat ~/.stack/config.yaml_ | grep -zoP 'templates:\n  params:\n#    author-name:\n#    author-email:\n#    copyright:\n#    github-username:' && echo FOUND  || echo NOTFOUND
#cat ~/.stack/config.yaml_ | grep -zoP \
#'templates:\n  params:\n''#    author-name:\n'\
#'#    author-email:\n#    copyright:\n#    github-username:\n'

function stackConfig()
{
  echo "Configurando ~/.stack/config.yaml"
  cat ~/.stack/config.yaml | grep -zoP \
'templates:\n'\
'  params:\n'\
'#    author-name:\n'\
'#    author-email:\n'\
'#    copyright:\n'\
'#    github-username:\n'
  if [ "$?" -eq 0 ] ; then
    echo
    echo "'stack' necesita su nombre y apellidos y su direcci\u00f3n de correo electr\u00f3nico."
    getNameAndEmail

    sed -i -z 's/'\
'templates:\n'\
'  params:\n'\
'#    author-name:\n'\
'#    author-email:\n'\
'#    copyright:\n'\
'#    github-username:/'\
'templates:\n\  params:\n'\
"    author-name: ${NAME}\n"\
"    author-email: ${EMAIL}\n"\
"    copyright: ${COPYRIGHT}\n"\
"    github-username: ${GITHUB_USERNAME}/" ~/.stack/config.yaml
  fi
}

function _installstack()
{
  wget -qO- https://get.haskellstack.org/ | sh
}

echo -e "

${B}21) Instalar stack${b} (Entorno de trabajo con Haskell)
[pulsa 'S', 's' o 'Retorno' para instalar,
       'n' para saltar la instalaci\u00f3n, o
       'a' para abortar]
wget -qO- https://get.haskellstack.org/ | sh"
processYNAbort_es "_installstack"

stackConfig

function fetchDependencies()
{
#cd /tmp/ ; stack new helloworld new-template --resolver ${RESOLVER} \
# -p "author-email:value" \
# -p "author-name:value" \
# -p "category:value" \
# -p "copyright:value" \
# -p "github-username:value" \
# build ; cd helloworld ; stack test

LIBROOT="/tmp"
LIB_L=helloworld
LIB_U=Helloworld

# https://docs.codewars.com/languages/haskell/
echo -e "${B}${LIB_U}${B_} LIB is being prepared."

cd "${LIBROOT}"

# Create and update files to force a compilation and run tests:
stack new "${LIB_L}" --resolver="${RESOLVER}"

cd "${LIBROOT}/${LIB_L}"

# https://stackoverflow.com/questions/5178828/how-to-replace-all-lines-between-two-points-and-subtitute-it-with-some-text-in-s

# - hspec-codewars # https://github.com/codewars/hspec-codewars
# - hspec-formatters-codewars # https://github.com/codewars/hspec-formatters-codewars

# Add dependencies for QuickCheck and hspec
sed -i '/^dependencies:/,/^- base >= 4.7 && < 5/'\
'c\dependencies:\n\- base >= 4.7 && < 5\n'\
'- array\n'\
'- bytestring\n'\
'- containers\n'\
'- heredoc\n'\
'- hscolour\n'\
'- polyparse\n'\
'- pretty-show\n'\
'- unordered-containers\n'\
'- Cabal\n'\
'- HUnit\n'\
'- QuickCheck\n'\
'- attoparsec\n'\
'- haskell-src-exts\n'\
'- hspec\n'\
'- hspec-attoparsec\n'\
'- hspec-contrib\n'\
'- hspec-megaparsec\n'\
'- HUnit-approx\n'\
'- lens\n'\
'- megaparsec\n'\
'- mtl\n'\
'- parsec\n'\
'- persistent\n'\
'- persistent-sqlite\n'\
'- persistent-template\n'\
'- random\n'\
'- regex-pcre\n'\
'- regex-posix\n'\
'- regex-tdfa\n'\
'- split\n'\
'- text\n'\
'- transformers\n'\
'- vector' "package.yaml"

# Add hspec-discover. Add five default extensions
sed -i '/^  source-dirs: src/,/^tests:'/\
'c\  source-dirs: src\n\nbuild-tools:\n- hspec-discover\n\n'\
'default-extensions:\n- InstanceSigs\n- TypeApplications\n'\
'- ScopedTypeVariables\n- GADTSyntax\n- PartialTypeSignatures\n\n'\
'- OverloadedStrings\n\n'\
'tests:' "package.yaml"

##### stack test

# Spec.hs:
cat > test/Spec.hs << EOF
{-# OPTIONS_GHC -F -pgmF hspec-discover #-}
EOF

# "${LIB_U}"Spec.hs:
cat > test/"${LIB_U}"Spec.hs << EOF
module ${LIB_U}Spec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import ${LIB_U}

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "Basic tests" $ do
    it "___DUMMY___" $ do
      id 42 \`shouldBe\` 42
EOF

# Lastly we'll add some shell content for the project to compile:
cat > src/${LIB_U}.hs << EOF
module ${LIB_U} where

${LIB_L} :: undefined
${LIB_L}  = undefined
EOF

# la siguiente l\u00ednea fuerza traer todas las librer\u00edas seleccionadas de internet, lo que lleva bastante tiempo
stack test
}

# Fetch haskell selected libraries at once
fetchDependencies

# Fetch emacs imports for emacs initialization files
emacs -l ~/.emacs.d/init.el emacs \
  --eval "(message \"Only warnings but no errors? That's OK! (Emacs will close by itself in 7 seconds).\")" \
  --eval "(run-at-time \"7 sec\" nil #'kill-emacs)"

echo -e "

Ha llegado hasta el \u00faltimo paso.

Tiene que salir de la sesi\u00f3n de escritorio y volver a entrar,
  antes de ejecutar el programa ${B}newk${b}.

Si es novato, le recomendamos que ejecute ${B}newk --help${b} y haga
  la kata ${B}multiply${b}, que ver\u00e1 al final de la ayuda.

${B}\u00a1Fin!${b}

"

echo "killall asciinema"
killall asciinema
sleep 2
